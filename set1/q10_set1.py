#Program to generate fibonacci sequence
def fib():
    num = int(input("How many fibonacci numbers would you like to generate? "))
    i = 1
    if num == 0: #user input zero return empty list
        fib = []
    elif num == 1:#user input one return single element in list
        fib = [1]
    elif num == 2:
        fib = [1,1]
    elif num > 2:
        fib = [1,1]
        while i < (num - 1):#iterate loop till it reaches num-1
            fib.append(fib[i] + fib[i-1])#adding numbers together to get the next number.
            i += 1
 
    return fib
